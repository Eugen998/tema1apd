#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <math.h>
#include <complex.h>
#include <string.h>

/* -------------------------------------------------------------------------- */
/*                                Declarations                                */
/* -------------------------------------------------------------------------- */
int N, P;
char inputPath[500], outputPath[500];
double *values;
complex double *resultValues, s = 0, exponent;

/* -------------------------------------------------------------------------- */
/*                Function for allocating memory for the arrays               */
/* -------------------------------------------------------------------------- */

void allocateMemory()
{
	values = malloc(N * sizeof(double));
	resultValues = malloc(N * sizeof(double complex));
}

/* -------------------------------------------------------------------------- */
/*                              Reading function                              */
/* -------------------------------------------------------------------------- */

void read(char *fileName)
{
	FILE *filePtrIn;
	filePtrIn = fopen(fileName, "r");

	/* --------------------------------- Read N --------------------------------- */

	fscanf(filePtrIn, "%d", &N);
	if (N == 0)
	{
		printf("Incorrect input file, abort;");
		return;
	}

	/* --------------- As soon as we read N we can allocate memory -------------- */

	allocateMemory();

	/* -------------------------- Read rest of elements ------------------------- */

	for (int i = 0; i < N; i++)
	{
		fscanf(filePtrIn, "%lf", &values[i]);
	}

	/* ---------------------------- Close input file ---------------------------- */

	fclose(filePtrIn);
}

/* -------------------------------------------------------------------------- */
/*                              Writing function                              */
/* -------------------------------------------------------------------------- */

void write(char *fileName)
{
	FILE *filePtrOut;
	filePtrOut = fopen(fileName, "w");
	fprintf(filePtrOut, "%d\n", N);
	for (int i = 0; i < N; i++)
	{
		fprintf(filePtrOut, "%lf %lf\n", creal(resultValues[i]), cimag(resultValues[i]));
	}
	fclose(filePtrOut);
}

/* -------------------------------------------------------------------------- */
/*                       Function for getting arguments                       */
/* -------------------------------------------------------------------------- */

void getArgs(int argc, char **argv)
{
	if (argc < 4)
	{
		printf("Not enough paramters: ./homeworkFT inputFile outputFile numThreads\n");
		exit(1);
	}
	strcpy(inputPath, argv[1]);
	strcpy(outputPath, argv[2]);
	P = atoi(argv[3]);
}

/* -------------------------------------------------------------------------- */
/*                               Thread Function                              */
/* -------------------------------------------------------------------------- */

void *threadFunction(void *args)
{
	int thread_id = *(int *)args;

	int start = thread_id * ceil((double)N / P);
	int end = fmin(N, (thread_id + 1) * ceil((double)N / P));

	for (int i = start; i < end; i++)
	{
		s = 0;
		for (int j = 0; j < N; j++)
		{
			exponent = (-1) * (2 * M_PI * j * i / N) * _Complex_I;
			s += values[j] * cexp(exponent);
		}
		resultValues[i] = s;
	}

	return 0;
}

/* -------------------------------------------------------------------------- */
/*                                    Main                                    */
/* -------------------------------------------------------------------------- */

int main(int argc, char *argv[])
{

	/* ------------------------ Declaration of variables ------------------------ */

	pthread_t tid[P];
	int i, thread_id[P];

	/* --------------- Get args from cli and read imput from file --------------- */

	getArgs(argc, argv);
	read(inputPath);

	/* ------------------ Initializing the id for every thread ------------------ */

	for (i = 0; i < P; i++)
		thread_id[i] = i;

	/* ------------- Creating threads and calling the threadFunction ------------ */

	for (i = 0; i < P; i++)
	{
		pthread_create(&(tid[i]), NULL, threadFunction, &(thread_id[i]));
	}

	/* ------------------------------ Join threads ------------------------------ */

	for (i = 0; i < P; i++)
	{
		pthread_join(tid[i], NULL);
	}

	/* ------------------------------ Write output ------------------------------ */

	write(outputPath);

	return 0;
}
