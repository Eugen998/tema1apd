#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <math.h>
#include <complex.h>
#include <string.h>

typedef double complex cplx;

/* -------------------------------------------------------------------------- */
/*                                Declarations                                */
/* -------------------------------------------------------------------------- */
int N, P;
char inputPath[500], outputPath[500];
cplx *values, *out, exponent;

/* -------------------------------------------------------------------------- */
/*                Function for allocating memory for the arrays               */
/* -------------------------------------------------------------------------- */

void allocateMemory()
{
	values = malloc((N + 4) * sizeof(cplx));
	out = malloc((N + 4) * sizeof(cplx));
}

/* -------------------------------------------------------------------------- */
/*                              Reading function                              */
/* -------------------------------------------------------------------------- */

void read(char *fileName)
{
	double aux;
	FILE *filePtrIn;
	filePtrIn = fopen(fileName, "r");

	/* --------------------------------- Read N --------------------------------- */

	fscanf(filePtrIn, "%d", &N);
	if (N == 0)
	{
		printf("Incorrect input file, abort;");
		return;
	}

	/* --------------- As soon as we read N we can allocate memory -------------- */

	allocateMemory();

	/* -------------------------- Read rest of elements ------------------------- */

	for (int i = 0; i < N; i++)
	{
		fscanf(filePtrIn, "%lf", &aux);
		values[i] = aux;
		out[i] = values[i];
	}

	/* ---------------------------- Close input file ---------------------------- */

	fclose(filePtrIn);
}

/* -------------------------------------------------------------------------- */
/*                              Writing function                              */
/* -------------------------------------------------------------------------- */

void write(char *fileName)
{
	FILE *filePtrOut;
	filePtrOut = fopen(fileName, "w");
	fprintf(filePtrOut, "%d\n", N);

	if (P == 1 || P == 4)
	{
		/* ----------- If we work on one or four threads we print the *values array ---------- */

		for (int i = 0; i < N; i++)
		{
			fprintf(filePtrOut, "%lf %lf\n", creal(values[i]), cimag(values[i]));
		}
	}
	else
	{

		/* ---------- If we work on two threads we print the *out array ---------- */

		for (int i = 0; i < N; i++)
		{
			fprintf(filePtrOut, "%lf %lf\n", creal(out[i]), cimag(out[i]));
		}
	}

	fclose(filePtrOut);
}

/* -------------------------------------------------------------------------- */
/*                       Function for getting arguments                       */
/* -------------------------------------------------------------------------- */

void getArgs(int argc, char **argv)
{
	if (argc < 4)
	{
		printf("Not enough paramters: ./homeworkFT inputFile outputFile numThreads\n");
		exit(1);
	}
	strcpy(inputPath, argv[1]);
	strcpy(outputPath, argv[2]);
	P = atoi(argv[3]);
}

/* -------------------------------------------------------------------------- */
/*                     FFT function (from rosettacode.org)                    */
/* -------------------------------------------------------------------------- */

void fft(cplx *values, cplx *out, int N, int step)
{
	if (step < N)
	{
		fft(out, values, N, step * 2);
		fft(out + step, values + step, N, step * 2);

		for (int i = 0; i < N; i += 2 * step)
		{
			cplx t = cexp(-_Complex_I * M_PI * i / N) * out[i + step];
			values[i / 2] = out[i] + t;
			values[(i + N) / 2] = out[i] - t;
		}
	}
	return;
}

/* -------------------------------------------------------------------------- */
/*                               Thread Function                              */
/* -------------------------------------------------------------------------- */

void *threadFunction(void *args)
{
	int thread_id = *(int *)args;

	if (P == 1)
	{
		fft(values, out, N, 1);
		//For P = 1 we call the function one time, with the normal arguments
	}
	else if (P == 2 || P == 4)
	{
		fft(values + thread_id, out + thread_id, N, P);
		//For P = 2 we call the function from 2 threads (thread_id = 0 || 1, P = 2)
		//the first thread the same as above, but with step = P = 2
		//the second thread starting from the second element
		//of each array, also with step = 2
		//For P = 4 we call the function from 4 threads (thread_id = 0 || 1 || 2 || 3, P = 4)
	}
	else
	{
		printf("Invalid number of threads. Use 1, 2 or 4.\n");
		exit(1);
	}
	return 0;
}

/* -------------------------------------------------------------------------- */
/*                                    Main                                    */
/* -------------------------------------------------------------------------- */

int main(int argc, char *argv[])
{

	/* ------------------------ Declaration of variables ------------------------ */

	pthread_t tid[P];
	int i, thread_id[P];

	/* --------------- Get args from cli and read imput from file --------------- */

	getArgs(argc, argv);
	read(inputPath);

	/* ------------------ Initializing the id for every thread ------------------ */

	for (i = 0; i < P; i++)
		thread_id[i] = i;

	/* ------------- Creating threads and calling the threadFunction ------------ */

	for (i = 0; i < P; i++)
	{
		pthread_create(&(tid[i]), NULL, threadFunction, &(thread_id[i]));
	}

	/* ------------------------------ Join threads ------------------------------ */

	for (i = 0; i < P; i++)
	{
		pthread_join(tid[i], NULL);
	}

	//If we use 2 or 4 threads we have to make sure we link the
	//results from the threads, by executing the last "for" statement
	//from the FFT function some more times.

	if (P == 2) //Execute just one time (thread1 + thread2)
	{
		for (int i = 0; i < N; i += 2)
		{
			cplx t = cexp(-_Complex_I * M_PI * i / N) * values[i + 1];
			out[i / 2] = values[i] + t;
			out[(i + N) / 2] = values[i] - t;
		}
	}
	else if (P == 4) //Execute three times ((thread1 + thread2) + (thread3 + thread4))
	{
		for (int i = 0; i < N; i += 4)
		{
			cplx t = cexp(-_Complex_I * M_PI * i / N) * values[i + 2];
			out[i / 2] = values[i] + t;
			out[(i + N) / 2] = values[i] - t;
		}

		for (int i = 0; i < N; i += 4)
		{
			cplx t = cexp(-_Complex_I * M_PI * i / N) * (values + 1)[i + 2];
			(out + 1)[i / 2] = (values + 1)[i] + t;
			(out + 1)[(i + N) / 2] = (values + 1)[i] - t;
		}

		for (int i = 0; i < N; i += 2)
		{
			cplx t = cexp(-_Complex_I * M_PI * i / N) * out[i + 1];
			values[i / 2] = out[i] + t;
			values[(i + N) / 2] = out[i] - t;
		}
	}

	/* ------------------------------ Write output ------------------------------ */

	write(outputPath);

	return 0;
}